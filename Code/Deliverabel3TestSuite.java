/*
 * Jeffrey Josephs
 * CS 1632 Deliverable 3
 * Test Suite
 */

import junit.framework.Test;
import junit.framework.TestSuite;

public class Deliverabel3TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite();
		
		/*
		 * User Story 1
		 * 
		 * As a user
		 * I want to search the threads by author
		 * So that I can find posts by the people I enjoy most
		 */
		suite.addTestSuite(TestExistingAuthor.class);
		suite.addTestSuite(TestUnknownAuthor.class);
		suite.addTestSuite(TestNoAuthor.class);
		
		/*
		 * User Story 2
		 * 
		 * As a user
		 * I want to search for posts by website and text in the URL
		 * So that I can find specific posts on a preferred website
		 */
		suite.addTestSuite(TestValidSiteAndUrlText.class);
		suite.addTestSuite(TestSitePartial.class);
		suite.addTestSuite(TestUrlText.class);
		suite.addTestSuite(TestSite.class);
		suite.addTestSuite(TestNoSiteAndNoUrlText.class);
		suite.addTestSuite(TestSiteAndUnknownText.class);
		suite.addTestSuite(TestUrlTextSpecialChar.class);
		suite.addTestSuite(TestSiteSpecialChar.class);
		
		/*
		 * User Story 3
		 * 
		 * As a user
		 * I want to be able to interact with the posts on the website
		 * So that I can alter the posts and subreddits I see
		 */
		 suite.addTestSuite(AllFrontPostsTest.class);
		 suite.addTestSuite(FrontPageSubscriptions.class);
		 suite.addTestSuite(SortingTest.class);
		 suite.addTestSuite(SubscribeTest.class);
		 suite.addTestSuite(UnsubscribeTest.class);
		 
		/*
		 * User Story 4
		 * 
		 * As a user
		 * I want to be able to change my preferences
		 * So that I can change the way I view posts
		 */
		 suite.addTestSuite(ChangeNumLinksTest.class);
		 suite.addTestSuite(HereThereBePiratesTest.class);
		 suite.addTestSuite(NSFWPropertiesTest.class);
		 suite.addTestSuite(ShowThumbnailTest.class);
		
		return suite;
	}

	public static void main(String[] args) {
		// Run the test suite
		junit.textui.TestRunner.run(suite());
	}

}
