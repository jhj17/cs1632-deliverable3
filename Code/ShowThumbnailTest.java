import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ShowThumbnailTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.reddit.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testShowThumbnail() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.name("user")).click();
    driver.findElement(By.name("user")).clear();
    driver.findElement(By.name("user")).sendKeys("testUser25");
    driver.findElement(By.name("passwd")).click();
    driver.findElement(By.name("passwd")).clear();
    driver.findElement(By.name("passwd")).sendKeys("testPass1");
    driver.findElement(By.cssSelector("button.btn")).click();
    driver.get(baseUrl + "/r/gifs/top/");
    driver.findElement(By.linkText("preferences")).click();
    driver.findElement(By.id("media_subreddit")).click();
    driver.findElement(By.id("media_off")).click();
    driver.findElement(By.cssSelector("input.btn")).click();
    driver.get(baseUrl + "/r/gifs");
    driver.findElement(By.linkText("top")).click();
    try {
      assertFalse(driver.findElement(By.xpath("//div[@id='thing_t3_2pvojg']/a/img")).isDisplayed());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.findElement(By.cssSelector("#thing_t3_2pvojg > div.entry.unvoted > p.title")).click();
    driver.findElement(By.linkText("preferences")).click();
    driver.findElement(By.id("media_subreddit")).click();
    driver.findElement(By.cssSelector("input.btn")).click();
    driver.findElement(By.linkText("logout")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
